#!/bin/sh

userresources=$HOME/.Xresources
usermodmap=$HOME/.Xmodmap

# merge in defaults and keymaps

if [ -f "$userresources" ]; then
    # X resources Usage xinitrc -
    # https://wiki.archlinux.org/index.php
    #       /x_resources#xinitrc
    xrdb -merge "$userresources"
fi

if [ -f "$usermodmap" ]; then
    # Qwerty-Lafayette - https://qwerty-lafayette.org
    setxkbmap fr -variant lafayette

    # xmodmap Activating the custom table -
    # https://wiki.archlinux.org/index.php
    #       /xmodmap#Activating_the_custom_table
    xmodmap "$usermodmap"
fi

# start some nice programs

if [ -d /etc/X11/xinit/xinitrc.d ] ; then
 for f in /etc/X11/xinit/xinitrc.d/?*.sh ; do
  [ -x "$f" ] && . "$f"
 done
 unset f
fi

# An extensible and highly configurable alternative
# to redshift - https://github.com/maandree/blueshift
blueshift --temperature 25500 &

# Unclutter Usage -
# https://wiki.archlinux.org/index.php/unclutter#Usage
# Alternatives xbanish -
# https://wiki.archlinux.org/index.php/unclutter#xbanish
xbanish &

# Xcompmgr Configuration -
# https://wiki.archlinux.org/index.php/xcompmgr#Configuration
xcompmgr -c -o 0 &

# Set background color
hsetroot -solid "#FFFFFF"

# Managers - 
# https://wiki.archlinux.org/index.php/clipboard#Managers
# This will keep the clipboard in sync with the primary buffer
autocutsel -selection CLIPBOARD -fork &
# This will keep the primary in sync with the clipboard buffer
autocutsel -selection PRIMARY -fork &

# Terminal daemon -
# https://wiki.archlinux.org/index.php/Rxvt-unicode
#       /Tips_and_tricks#Xinitrc
pgrep --exact urxvtd &>/dev/null \
   || urxvtd --quiet --opendisplay --fork
# Terminal emulator -
# https://wiki.archlinux.org/index.php/i3#Terminal_emulator
export TERMINAL=urxvtc

export PATH=$HOME/.usr/bin:$PATH

# i3 Starting From tty -
# https://wiki.archlinux.org/index.php/i3#From_tty
exec i3 &> /dev/null
