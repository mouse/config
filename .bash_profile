#
# ~/.bash_profile
#

# xinit - Autostart X at login ; https://wiki.archlinux.org/index.php/Xinit
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
    # Silent boot startx - https://wiki.archlinux.org/index.php/silent_boot
    [[ $(fgconsole 2>/dev/null) == 1 ]] && exec startx -- vt1 &> /dev/null
fi

[[ -f ~/.bashrc ]] && . ~/.bashrc
