-- vim: ts=4 sw=4 noet ai cindent syntax=lua
--[[
Conky, a system monitor, based on torsmo

Any original torsmo code is licensed under the BSD license

All code written since the fork of torsmo is licensed under the GPL

Please see COPYING for details

Copyright (c) 2004, Hannu Saransaari and Lauri Hakkarainen
Copyright (c) 2005-2012 Brenden Matthews, Philip Kovacs, et. al. (see AUTHORS)
All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

conky.config = {
	xinerama_head = 0,
	update_interval = 1,
	double_buffer = true,
	no_buffers = true,
	use_xft = true,
	font = 'monospace:pixelsize=11',
    background = true,
	draw_shades = false,
    own_window = true,
    own_window_type = 'normal',
    own_window_class = 'Conky',
    own_window_transparent = true,
    own_window_hints = 'undecorated,sticky,skip_taskbar,skip_pager,below',
    own_window_argb_visual = true,
    own_window_argb_value = 0,
	minimum_width = 200,
	minimum_height = 80,
    alignment = 'bottom_middle',
	gap_y = 0,
	default_color = '#000000',

	cpu_avg_samples = 2,
	net_avg_samples = 2,

	lua_load = '~/.config/conky/script.lua',
	lua_draw_hook_pre = 'conky_main',
}

conky.text = [[]]
