# i3 config file (v4) - https://i3wm.org/docs/userguide.html

set $sup Mod1
set $mod Mod4

set $font さざなみゴシック
set $size 8
set $facesize 19

set $geom 141x15
set $geom_abook 142x20
set $geom_terminal 142x15

default_border pixel 0
default_floating_border pixel 0
hide_edge_borders both
gaps inner 3

client.focused          #FFFFFF #FFFFFF #000000 
client.unfocused        #FFFFFF #FFFFFF #000000 
client.focused_inactive #FFFFFF #FFFFFF #000000 
client.urgent           #FFFFFF #000000 #FFFFFF 
client.placeholder      #000000 #000000 #FFFFFF 

client.background       #FFFFFF

exec --no-startup-id i3-sensible-terminal

# Tabbed or stacked web-browsing
# https://wiki.archlinux.org/index.php
#       /i3#Tabbed_or_stacked_web-browsing
for_window [class="Vimb"] focus child, layout tabbed, focus

# Scratchpad programs
for_window [instance="_pad$"] \
           floating enable, move position 3 20, move scratchpad
for_window [instance="terminal_pad"] \
           move position -2 450, move scratchpad

exec --no-startup-id i3-sensible-terminal -name terminal_pad \
           -rv -fn "xft:$font:pixelsize=$facesize" \
           -geometry $geom_terminal
bindsym $sup+shift+F1 [instance="terminal_pad"] scratchpad show

exec --no-startup-id i3-sensible-terminal -name cmus_pad \
           -fn "xft:$font:pixelsize=$facesize" \
           -geometry $geom -e cmus
bindsym $sup+F2 [instance="cmus_pad"] scratchpad show

exec --no-startup-id i3-sensible-terminal -name mutt_pad \
           -fn "xft:$font:pixelsize=$facesize" \
           -geometry $geom -e mutt
bindsym $sup+F3 [instance="mutt_pad"] scratchpad show
 
exec --no-startup-id i3-sensible-terminal -name abook_pad \
           -fn "xft:$font:pixelsize=$facesize" \
           -geometry $geom_abook -e abook
bindsym $sup+F4 [instance="abook_pad"] scratchpad show
 
# Font for window titles. Will also be used by the bar unless
# a different font is used in the bar {} block below.
font pango:$font $size

# use these keys for focus, movement, and resize directions
# when reaching for the arrows is not convenient
set $up k
set $down j
set $left h
set $right l

#Transparency
bindsym $sup+i exec transset-df --actual --dec 0.1
bindsym $sup+Shift+i exec transset-df --actual --inc 0.1

# Take a screenshot upon pressing $sup+x (select an area)
bindsym --release $sup+x exec import /tmp/screenshot.png
#bindsym --release $sup+Shift+x exec import

# Pulse Audio controls
bindsym XF86AudioRaiseVolume exec pactl set-sink-volume 0 +5%
bindsym XF86AudioLowerVolume exec pactl set-sink-volume 0 -5%
bindsym XF86AudioMute exec pactl set-sink-mute 0 toggle

# Media player controls
bindsym XF86AudioPause exec cmus-remote --pause
bindsym XF86AudioNext exec cmus-remote --next
bindsym XF86AudioPrev exec cmus-remote --prev

# use Mouse+$sup to drag floating windows
# to their wanted position
floating_modifier $sup

# start a terminal
bindsym $sup+Return exec i3-sensible-terminal
bindsym $sup+Shift+Return exec xterm

# start a browser
bindsym $sup+t exec vimb

# light
bindsym XF86MonBrightnessDown exec xbacklight -dec 5
bindsym XF86MonBrightnessUp exec xbacklight -inc 5

# lock the window manager
bindsym XF86Launch1 exec lockscreen

# kill focused window
bindsym $sup+Shift+q kill

# start dmenu (a program launcher)
bindsym $mod+d exec dmenu_run -fn $font \
            -nb "#000000" -nf "#FFFFFF" \
            -sf "#000000" -sb "#FFFFFF"

# change focus
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

# move focused window
bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

# split in horizontal orientation
bindsym $sup+h split h

# split in vertical orientation
bindsym $sup+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle
bindsym $mod+Shift+f bar mode toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $sup+s layout stacking
bindsym $sup+w layout tabbed
bindsym $sup+e layout toggle split

# toggle tiling / floating
bindsym $sup+space floating toggle

# change focus between tiling / floating windows
bindsym $sup+shift+space focus mode_toggle

# focus the parent container
bindsym $sup+a focus parent

# focus the child container
#bindsym $sup+d focus child

# move the currently focused window to the scratchpad
bindsym $sup+Shift+minus move scratchpad

# Show the next scratchpad window or hide the focused
# scratchpad window. If there are multiple scratchpad
# windows, this command cycles through them.
bindsym $sup+minus scratchpad show

# Define names for default workspaces for which we configure
# key bindings later on. We use variables to avoid repeating
# the names in multiple places.
set $ws1 "1:①"
set $ws2 "2:②"
set $ws3 "3:③"
set $ws4 "4:④"
set $ws5 "5:⑤"
set $ws6 "6:⑥"
set $ws7 "7:⑦"
set $ws8 "8:⑧"
set $ws9 "9:⑨"
set $ws10 "10:⑩"
set $ws11 "11:⑪"
set $ws12 "12:⑫"
set $ws13 "13:⑬"
set $ws14 "14:⑭"
set $ws15 "15:⑮"
set $ws16 "16:⑯"
set $ws17 "17:⑰"
set $ws18 "18:⑱"
set $ws19 "19:⑲"
set $ws20 "20:⑳"

# switch to workspace
bindsym $sup+1 workspace $ws1
bindsym $sup+2 workspace $ws2
bindsym $sup+3 workspace $ws3
bindsym $sup+4 workspace $ws4
bindsym $sup+5 workspace $ws5
bindsym $sup+6 workspace $ws6
bindsym $sup+7 workspace $ws7
bindsym $sup+8 workspace $ws8
bindsym $sup+9 workspace $ws9
bindsym $sup+0 workspace $ws10
bindsym $sup+Shift+1 workspace $ws11
bindsym $sup+Shift+2 workspace $ws12
bindsym $sup+Shift+3 workspace $ws13
bindsym $sup+Shift+4 workspace $ws14
bindsym $sup+Shift+5 workspace $ws15
bindsym $sup+Shift+6 workspace $ws16
bindsym $sup+Shift+7 workspace $ws17
bindsym $sup+Shift+8 workspace $ws18
bindsym $sup+Shift+9 workspace $ws19
bindsym $sup+Shift+0 workspace $ws20

# move focused container to workspace
bindsym $mod+1 move container to workspace $ws1
bindsym $mod+2 move container to workspace $ws2
bindsym $mod+3 move container to workspace $ws3
bindsym $mod+4 move container to workspace $ws4
bindsym $mod+5 move container to workspace $ws5
bindsym $mod+6 move container to workspace $ws6
bindsym $mod+7 move container to workspace $ws7
bindsym $mod+8 move container to workspace $ws8
bindsym $mod+9 move container to workspace $ws9
bindsym $mod+0 move container to workspace $ws10
bindsym $mod+Shift+1 move container to workspace $ws11
bindsym $mod+Shift+2 move container to workspace $ws12
bindsym $mod+Shift+3 move container to workspace $ws13
bindsym $mod+Shift+4 move container to workspace $ws14
bindsym $mod+Shift+5 move container to workspace $ws15
bindsym $mod+Shift+6 move container to workspace $ws16
bindsym $mod+Shift+7 move container to workspace $ws17
bindsym $mod+Shift+8 move container to workspace $ws18
bindsym $mod+Shift+9 move container to workspace $ws19
bindsym $mod+Shift+0 move container to workspace $ws20

# reload the configuration file
bindsym $sup+Shift+c reload
# restart i3 inplace
# (preserves your layout/session, can be used to upgrade i3)
bindsym $sup+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $sup+Shift+e exec "i3-nagbar --font $font \
           --message 'Exit shortcut!' --button 'Yes, exit i3' \
                                               'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
    # These bindings trigger as soon as you enter
    # the resize mode

    # Pressing left will shrink the window’s width.
    # Pressing right will grow the window’s width.
    # Pressing up will shrink the window’s height.
    # Pressing down will grow the window’s height.
    bindsym $left       resize shrink width 10 px or 10 ppt
    bindsym $down       resize grow height 10 px or 10 ppt
    bindsym $up         resize shrink height 10 px or 10 ppt
    bindsym $right      resize grow width 10 px or 10 ppt

    # same bindings, but for the arrow keys
    bindsym Left        resize shrink width 10 px or 10 ppt
    bindsym Down        resize grow height 10 px or 10 ppt
    bindsym Up          resize shrink height 10 px or 10 ppt
    bindsym Right       resize grow width 10 px or 10 ppt

    # back to normal: Enter or Escape or $sup+r
    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $sup+r mode "default"
}

bindsym $sup+r mode "resize"

bar {
    set $barfont さざなみゴシック
    set $barsize 8

    font pango:$barfont $barsize

    status_command i3status
    i3bar_command i3bar -t

    output LVDS1
    position top
    workspace_buttons yes
    strip_workspace_numbers yes
    tray_output none

    colors {
        background #FFFFFF
        statusline #000000
        separator #FFFFFF

        focused_workspace #000000 #FFFFFF #000000
        active_workspace #FFFFFF #FFFFFF #000000
        inactive_workspace #FFFFFF #FFFFFF #000000
        urgent_workspace #FFFFFF #FFFFFF #000000
    }
}
